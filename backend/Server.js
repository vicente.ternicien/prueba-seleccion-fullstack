const express = require('express');
const app = express();
const mongodb = require('mongodb');
const PORT = 4000;
const config = require('./db');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const cors = require ('cors');
const bodyParser = require('body-parser')



var tempdata;
const client = mongodb.MongoClient;
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


//Load the data the first time
var ourRequest = new XMLHttpRequest()
ourRequest.open('GET', 'https://api.got.show/api/book/characters')
ourRequest.onload = dataLoad
ourRequest.send();

  //------------------------------------//
 //---------------ROUTES---------------//
//------------------------------------//

app.get('/', function(req, res) {
    res.send(tempdata.hits);
});

// here is where the client gets its data, 
// it should only return data that wasn't deleted by the user.
app.get('/news', function(req, res) {
    var resultArray = [];
    client.connect(config.DB, { useUnifiedTopology: true }, function(err, client) {
        var db = client.db('mynewsdb')
        var cursor = db.collection('news-data');
        cursor.forEach(function(element, err) {
            if (element.isdeleted === false){
                resultArray.push(element);
            }
        }, function(){
            client.close();
            res.send(resultArray);
        });
    });
});
//Gives the Tag Deleted to an item.
//It dosent delete the item from the DB, because it would be reloaded once an hour
app.get('/delete/:id', function(req, res) {
  var resultArray = [];
  var item = { isdeleted: true };
  client.connect(config.DB, { useUnifiedTopology: true }, function(err, client) {
      var db = client.db('mynewsdb');
      db.collection("news-data").updateOne({"objectID": req.params.id}, {$set: item}, function(err, res) {
        if (err) throw err;
        client.close();
      });
  });
});

//Function to add the data to the DB
function dataLoad(){
    var resultArray = [];
        client.connect(config.DB, { useUnifiedTopology: true }, function(err, client) {
        var db = client.db('mynewsdb');
        var cursor = db.collection('news-data').find();
        cursor.forEach(element => {
            resultArray.push(element);
        },function(){
            tempdata.hits.forEach(dataElement => {        
                var duplicated = 0;
                //Check if there are duplicates in the Database, by matching their object ID's.
                //it also check the new aditions for null titles.
                resultArray.forEach(element => {
                    if (element.objectID !== dataElement.objectID){
                        duplicated++;
                    }
                    else {duplicated--;}
                    if (duplicated === resultArray.length) {
                        db.collection('news-data').insertOne(dataElement, function (err, result) {
                            console.log('item inserted');
                        });         
                    }
                }); 
            }, function(){
                client.close();
            });    
        });
    });
    return newsdata;
}



//open the server port, port 4000
app.listen(PORT, function(){
    console.log('Your node js server is running on PORT:',PORT);
});

module.exports = app;
module.exports = dataload, dataRefresh;